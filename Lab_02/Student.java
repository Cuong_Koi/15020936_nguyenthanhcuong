
public class Student {
    private String name;
    private int id;
    private String group;
    private String email;

    public Student(){
        this.name = "Student";
        this.id = 000;
        this.group = "K60CB";
        this.email = "uet@vnu.edu.vn";
    }
    
    public Student(String name, int id, String group, String email) {
        this.name = name;
        this.id = id;
        this.group = group;
        this.email = email;
    }
    
    public Student(Student st){
        this.name = st.name;
        this.id = st.id;
        this.group = st.group;
        this.email = st.email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInfo(){
        StringBuilder sb = new StringBuilder();
        sb.append("Name : ").append(this.name).append("\nId : ").append(this.id)
                .append("\nGroup: ").append(this.group).append("\nEmail: ").append(this.email);
        return sb.toString();
    }
}