
public class StudentManagement {
    public static void main(String[] args) {
        StudentManagement run = new StudentManagement();
        Student std1 = new Student("Nguyen Thanh Cuong", 15020936, "K60-CB", "cuong30061997@gmail.com");
        Student std2 = new Student();
        Student std3 = new Student(std1);
        Student std5 = new Student("CuongAcbd", 3, "K60-CB", "cuong30061997");
        Student std6 = new Student("CuongAcbed", 4, "K60-CD", "cuong30061997");
        System.out.println(std1.getName());
        System.out.println(std1.getInfo());
        System.out.println(run.doCompare(std1, std6));
    }

    public boolean sameGroup(Student std1, Student std2){
        return std1.getGroup().equals(std2.getGroup());
    }
    public String doCompare(Student std1, Student std2) {
        if(sameGroup(std1, std2)){
            return "Same !";
        }
        else{
            return "Not same !";
        }
    }
   
}
