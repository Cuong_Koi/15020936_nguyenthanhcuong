
import java.io.Serializable;

public class Triangle extends Shape implements ShapeInteface , Serializable {
    private Coordinates A;
    private Coordinates B;
    private Coordinates C;

    public Triangle() {
        super();
        A = new Coordinates(-1, 1);
        B = new Coordinates( 1, 1);
        C = new Coordinates(-1, -1);
    }

    public Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
        super();
        A = new Coordinates(x1, y1);
        B = new Coordinates(x2, y2);
        C = new Coordinates(x3, y3);
    }

    public Triangle(double x1, double y1, double x2, double y2, double x3, double y3, String color, boolean filled) {
        super(color, filled);
        A = new Coordinates(x1, y1);
        B = new Coordinates(x2, y2);
        C = new Coordinates(x3, y3);
    }

    public Coordinates getA() {
        return A;
    }

    public void setA(Coordinates a) {
        A = a;
    }

    public Coordinates getB() {
        return B;
    }

    public void setB(Coordinates b) {
        B = b;
    }

    public Coordinates getC() {
        return C;
    }

    public void setC(Coordinates c) {
        C = c;
    }

    public String toString() {
        return super.toString();
    }

    @Override
    public double getArea() {
        double p = (this.getLength(A,B) + this.getLength(B,C) + this.getLength(C,A))/2;
        return Helper.round(Math.sqrt(p*(p-this.getLength(A,B))*(p-this.getLength(B,C))*(p-this.getLength(C,A))));
    }

    @Override
    public double getPerimeter() {
        return Helper.round(this.getLength(A,B) + this.getLength(B,C) + this.getLength(C,A));
    }

    @Override
    public boolean validate() {
        double a = this.getLength(A,B);
        double b = this.getLength(B,C);
        double c = this.getLength(C,A);
//        System.out.println( a+b > c && b+c > a && a+c > b);
        if( a+b > c && b+c > a && a+c > b) return true;
        return false;
    }

    @Override
    public String fail() {
        StringBuilder s = new StringBuilder();
        s.append("A("+A.getX()+":"+A.getY()+") - ");
        s.append("B("+B.getX()+":"+B.getY()+") - ");
        s.append("C("+C.getX()+":"+C.getY()+")");
        s.append(" -> Validate triangle fail !");
        return  s.toString();
    }

    @Override
    public boolean isSame(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triangle triangle = (Triangle) o;

        if (A != null ? !A.equal(triangle.A) : triangle.A != null) return false;
        if (B != null ? !B.equal(triangle.B) : triangle.B != null) return false;
        return C != null ? C.equal(triangle.C) : triangle.C == null;
    }

    public void print() {
        super.print();
        System.out.println("type: Triangle");
        System.out.println("Area: " + this.getArea() + " - Perimeter: " + this.getPerimeter());
    }


}
