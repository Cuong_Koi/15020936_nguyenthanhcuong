
import java.io.Serializable;
import java.util.*;

public class Diagram implements Serializable {
    private ArrayList<Layer> listLayer;
    private HashMap<String, ArrayList<Shape>> hashMap;

    public HashMap<String, ArrayList<Shape>> getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap<String, ArrayList<Shape>> hashMap) {
        this.hashMap = hashMap;
    }

    public Diagram() {
        listLayer = new ArrayList<Layer>();
        hashMap = new HashMap<>();
    }

    public ArrayList<Layer> getListLayer() {
        return listLayer;
    }

    public void setListLayer(ArrayList<Layer> listLayer) {
        this.listLayer = listLayer;
    }

    public boolean addLayer(Layer layer) {
        return listLayer.add(layer);
    }

    public boolean delLayer(Layer layer) {
        return listLayer.remove(layer);
    }

    public boolean removeCircle() {
        for (Layer layer : listLayer) {
            for (Iterator<Shape> run = layer.getListShape().iterator(); run.hasNext(); ) {
                Shape shape = run.next();
                if (shape instanceof Circle) {
                    run.remove();
                }
            }
        }
        return true;
    }

    public void classifiShape(){
        if (listLayer.size() == 0) return;
        for (Layer layer : listLayer) {
            for (Iterator<Shape> run = layer.getListShape().iterator(); run.hasNext(); ) {
                Shape shape = run.next();
//                map.getKey().toString().substring(map.getKey().toString().lastIndexOf('.')+1,map.getKey().toString().length())
                String scl = shape.getClass().toString().substring(shape.getClass().toString().lastIndexOf('.')+1, shape.getClass().toString().length());
                if(hashMap.containsKey(scl)){
                    hashMap.get(scl).add(shape);
                }
                else{
                    ArrayList<Shape> abc = new ArrayList<>();
                    abc.add(shape);
                    hashMap.put(scl, abc);
                }
            }
        }
    }

    public void print() {
        if (listLayer.isEmpty()){
            System.out.println("This Diagram is empty !!");
            return;
        }
        Set set = hashMap.entrySet();

        for (Iterator list  = set.iterator(); list.hasNext(); ) {
            Map.Entry map = (Map.Entry) list.next();
            System.out.println();
            System.out.println("This is Layer of "+ map.getKey());
            ArrayList<Shape> list1 = (ArrayList<Shape>)map.getValue();
            for (Iterator<Shape> shapeIterator = list1.iterator() ; shapeIterator.hasNext();
                 ) {
                Shape shape = shapeIterator.next();
                shape.print();
            }
        }
    }
}
