

import java.io.Serializable;

/**
 * Created by CuongK60UET on 7/22/2017.
 */
public class Hexagon extends Shape implements ShapeInteface, Serializable {

    private Coordinates A;
    private Coordinates B;
    private Coordinates C;
    private Coordinates D;
    private Coordinates E;
    private Coordinates F;

    public Hexagon() {
        super();
        double a  = Math.sqrt(3);
        A = new Coordinates(-2, 0);
        B = new Coordinates(-1, a);
        C = new Coordinates(1, a);
        D = new Coordinates(0, 2);
        E = new Coordinates(1, -a);
        F = new Coordinates(-1, -a);
    }

    public Hexagon(double x1, double y1, double x2, double y2, double x3, double y3) {
        super();
        A = new Coordinates(x1, y1);
        B = new Coordinates(x2, y2);
        C = new Coordinates(x3, y3);
        D = new Coordinates(2 * (x1 + x3 - x2) - x1, 2 * (y1 + y3 - y2) - y1);
        E = new Coordinates(2 * (x1 + x3 - x2) - x2, 2 * (y1 + y3 - y2) - y2);
        F = new Coordinates(2 * (x1 + x3 - x2) - x3, 2 * (y1 + y3 - y2) - y3);
    }

    public Hexagon(double x1, double y1, double x2, double y2, double x3, double y3, String color, boolean filled) {
        super(color, filled);
        A = new Coordinates(x1, y1);
        B = new Coordinates(x2, y2);
        C = new Coordinates(x3, y3);
        D = new Coordinates(2 * (x1 + x3 - x2) - x1, 2 * (y1 + y3 - y2) - y1);
        E = new Coordinates(2 * (x1 + x3 - x2) - x2, 2 * (y1 + y3 - y2) - y2);
        F = new Coordinates(2 * (x1 + x3 - x2) - x3, 2 * (y1 + y3 - y2) - y3);
    }

    public Coordinates getA() {
        return A;
    }

    public void setA(Coordinates a) {
        A = a;
    }

    public Coordinates getB() {
        return B;
    }

    public void setB(Coordinates b) {
        B = b;
    }

    public Coordinates getC() {
        return C;
    }

    public void setC(Coordinates c) {
        C = c;
    }

    public Coordinates getD() {
        return D;
    }

    public void setD(Coordinates d) {
        D = d;
    }

    public Coordinates getE() {
        return E;
    }

    public void setE(Coordinates e) {
        E = e;
    }

    public Coordinates getF() {
        return F;
    }

    public void setF(Coordinates f) {
        F = f;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public double getPerimeter() {
        return 6*getLength(A,B);
    }

    @Override
    public double getArea() {
        return (double) (3*Math.sqrt(3)*Math.pow(getLength(A,B), 2)/2);
    }

    @Override
    public boolean validate() {
        double a = Helper.round(getLength(A,B));
        double b = Helper.round(getLength(B,C));
        double c = Helper.round(getLength(C,A));
        if (a != b) return false;
        if (c != Helper.round(Math.sqrt(3)*a)) return false;
        return true;
    }

    @Override
    public String fail() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("This Hexagon -> A("+A.getX()+":"+A.getY()+")-");
        stringBuilder.append("B("+B.getX()+":"+B.getY()+")-");
        stringBuilder.append("C("+C.getX()+":"+C.getY()+")-");
        stringBuilder.append(" isn't validate !");
        return stringBuilder.toString();
    }

    @Override
    public boolean isSame(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hexagon hexagon = (Hexagon) o;

        if (A != null ? !A.equal(hexagon.A) : hexagon.A != null) return false;
        if (B != null ? !B.equal(hexagon.B) : hexagon.B != null) return false;
        if (C != null ? !C.equal(hexagon.C) : hexagon.C != null) return false;
        if (D != null ? !D.equal(hexagon.D) : hexagon.D != null) return false;
        if (E != null ? !E.equal(hexagon.E) : hexagon.E != null) return false;
        return F != null ? F.equal(hexagon.F) : hexagon.F == null;
    }

    @Override
    public void print() {
        super.print();
        System.out.println("type: Hexagon");
        System.out.println("Area: " + Helper.round(this.getArea()) + " - Perimeter: " + Helper.round(this.getPerimeter()));
    }
}
