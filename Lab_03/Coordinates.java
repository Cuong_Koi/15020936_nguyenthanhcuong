

import java.io.Serializable;

public class Coordinates implements Serializable {
    private double x;
    private double y;

    public Coordinates(double x, double y) {
        this.x = Helper.round(x);
        this.y = Helper.round(y);
    }
    public Coordinates(){
        this.x = 0;
        this.y = 0;
    }
    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    public String getinfo(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("("+x+":"+y+")");
        return stringBuilder.toString();
    }
    public boolean equal(Coordinates coordinates){
        if (x != coordinates.getX() || y != coordinates.getY()) return false;
        return true;
    }
}
