
import java.io.Serializable;

public class Shape implements ShapeInteface, Serializable {
    private String color;
    private boolean filled;

    public Shape() {
        this.color = "Red";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Color: "+ this.color).append("\nFilled: " + this.filled);
        return stringBuilder.toString();
    }

    @Override
    public double getPerimeter() {
        return 0;
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public boolean validate() {
        return false;
    }

    @Override
    public String fail() {
        return null;
    }
    public double getLength(Coordinates a, Coordinates b){
        return Math.sqrt(Math.pow(a.getX() - b.getX(),2) + Math.pow(a.getY() - b.getY(), 2));
    }

    @Override
    public boolean isSame(Object b) {
        return false;
    }

    public void print(){
        System.out.println("------------------------------");
        System.out.println(this.toString());
    }
}
