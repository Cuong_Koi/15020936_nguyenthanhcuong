
import java.io.Serializable;

public class Rectangle extends Shape implements ShapeInteface, Serializable  {

    private Coordinates A;
    private Coordinates B;
    private Coordinates C;
    private Coordinates D;

    public Rectangle() {
        super();
        A = new Coordinates(-1, 1);
        B = new Coordinates(1, 1);
        C = new Coordinates(1, -1);
        D = new Coordinates(-1, -1);
    }

    public Rectangle(double x1, double y1, double x2, double y2, double x3, double y3) {
        super();
        A = new Coordinates(x1, y1);
        B = new Coordinates(x2, y2);
        C = new Coordinates(x3, y3);
        D = new Coordinates(2*(x1+x3)-x2, 2*(y1+y3)-y2);
    }

    public Rectangle(double x1, double y1, double x3, double y3) {
        super();
        A = new Coordinates(x1, y1);
        C = new Coordinates(x3, y3);
        double t = Helper.round(getLength(A,C)/2);
        double a = (x1+x3)/2;
        double b = (y1+y3)/2;
        double c = x1 - x3;
        double d = y1 - y3;
        B = new Coordinates(a+t*Math.sqrt((double)Math.pow(d, 2)/(Math.pow(d,2)+Math.pow(c, 2))), b-((double)c*t/d)*Math.sqrt((double)Math.pow(d, 2)/(Math.pow(d,2)+Math.pow(c, 2))));
        D = new Coordinates(a-t*Math.sqrt((double)Math.pow(d, 2)/(Math.pow(d,2)+Math.pow(c, 2))), b+((double)c*t/d)*Math.sqrt((double)Math.pow(d, 2)/(Math.pow(d,2)+Math.pow(c, 2))));
    }

    public Rectangle(double x1, double y1, double x2, double y2, double x3, double y3, String color, boolean filled) {
        super(color, filled);
        A = new Coordinates(x1, y1);
        B = new Coordinates(x2, y2);
        C = new Coordinates(x3, y3);
        D = new Coordinates(2*(x1+x3)-x2, 2*(y1+y3)-y2);
    }

    public Rectangle(double x1, double y1, double x3, double y3, String color, boolean filled) {
        super(color, filled);
        A = new Coordinates(x1, y1);
        C = new Coordinates(x3, y3);
        double t = Helper.round(getLength(A,C)/2);
        double a = (x1+x3)/2;
        double b = (y1+y3)/2;
        double c = x1 - x3;
        double d = y1 - y3;
        B = new Coordinates(a+t*Math.sqrt((double)Math.pow(d, 2)/(Math.pow(d,2)+Math.pow(c, 2))), b+((double)c*t/d)*Math.sqrt((double)Math.pow(d, 2)/(Math.pow(d,2)+Math.pow(c, 2))));
        D = new Coordinates(a-t*Math.sqrt((double)Math.pow(d, 2)/(Math.pow(d,2)+Math.pow(c, 2))), b-((double)c*t/d)*Math.sqrt((double)Math.pow(d, 2)/(Math.pow(d,2)+Math.pow(c, 2))));
    }

    public Coordinates getA() {
        return A;
    }

    public void setA(Coordinates a) {
        A = a;
    }

    public Coordinates getB() {
        return B;
    }

    public void setB(Coordinates b) {
        B = b;
    }

    public Coordinates getC() {
        return C;
    }

    public void setC(Coordinates c) {
        C = c;
    }

    public Coordinates getD() {
        return D;
    }

    public void setD(Coordinates d) {
        D = d;
    }

    public double getArea() {
        double a = Helper.round(getLength(A, B));
        double b = Helper.round(getLength(B, C));
        return a*b;
    }

    public double getPerimeter() {
        double a = Helper.round(getLength(A, B));
        double b = Helper.round(getLength(B, C));
        return 2*(a+b);
    }

    public String toString() {
        return super.toString();
    }

    public boolean validate() {
        if (((A.getX() - B.getX()) * (C.getX() - B.getX()) + (A.getY() - B.getY()) * (C.getY() - B.getY())) == 0) {
            return true;
        }
        return false;
    }

    public String fail(){
        StringBuilder s = new StringBuilder();
        s.append("A("+A.getX()+":"+A.getY()+") - ");
        s.append("B("+B.getX()+":"+B.getY()+") - ");
        s.append("C("+C.getX()+":"+C.getY()+")");
        s.append(" -> Validate fail !");
        return  s.toString();
    }

    @Override
    public boolean isSame(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (A != null ? !A.equal(rectangle.A) : rectangle.A != null) return false;
        if (B != null ? !B.equal(rectangle.B) : rectangle.B != null) return false;
        if (C != null ? !C.equal(rectangle.C) : rectangle.C != null) return false;
        return D != null ? D.equals(rectangle.D) : rectangle.D == null;
    }

    public void print() {
        super.print();
        System.out.println("type: Retangle");
        System.out.println("Area: " + this.getArea() + " - Perimeter: " + this.getPerimeter());
    }

}
