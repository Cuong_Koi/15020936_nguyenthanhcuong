

public interface ShapeInteface {
    public double getArea();

    public double getPerimeter();

    public void print();

    public boolean validate();

    public String fail();

    public boolean isSame(Object b);
}
