

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Layer implements Serializable {
    private boolean visible;
    ArrayList<Shape> listShape;

    public Layer() {
        visible = true;
        listShape = new ArrayList<Shape>();
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public ArrayList<Shape> getListShape() {
        return listShape;
    }

    public void setListShape(ArrayList<Shape> listShape) {
        this.listShape = listShape;
    }

    public boolean addShape(Shape shape) {
        if (shape.validate()){
            listShape.add(shape);
            return true;
        }
        return false;
    }

    public boolean delShape(Shape shape) {
        return listShape.remove(shape);
    }

    public int getCount() {
        return listShape == null ? 0 : listShape.size();
    }

    public void removeSame(){
        for (int i = getCount()-1; i >= 1; i--){
            for (int j = i-1 ; j>=0; j-- ){
//                System.out.println(i +" "+ j);
                if (listShape.get(i).isSame(listShape.get(j))){
                    listShape.remove(j);
                    i--;
                }
            }
        }
    }

    public boolean removeTriangle() {

        for (Iterator<Shape> run = listShape.iterator(); run.hasNext(); ) {
            Shape shape = run.next();
            if (shape instanceof Triangle) {
                run.remove();
            }
        }
        return true;
    }

    public void print() {
        if (this.visible){
            if (listShape.isEmpty()) System.out.println("This Layer is empty !!");
            else {
                for (Shape shape : listShape
                        ) {
                    shape.print();
                }
            }
        }

    }
}
