
import java.io.Serializable;

public class Square extends Rectangle implements ShapeInteface , Serializable {
    public Square() {
        super();
    }

    public Square(double xA, double yA, double xC, double yC) {
        super(xA, yA, xC, yC);
    }

    public Square(double xA, double yA, double xC, double yC, String color, boolean filled) {
        super(xA, yA, xC, yC);
    }


    public String toString() {
        return super.toString();
    }

    @Override
    public double getArea() {
        return super.getArea();
    }

    @Override
    public double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    public boolean validate() {
        return super.validate();
    }

    @Override
    public boolean isSame(Object b) {
        return super.isSame(b);
    }

    @Override
    public String fail() {
        return super.fail();
    }

    public void print() {
        System.out.println("------------------------");
        System.out.println(this.toString());
        System.out.println("type: Square");
        System.out.println("Area: " + this.getArea() + " - Perimeter: " + this.getPerimeter());
    }

}
