
public class Main {
    public static void main(String[] args) {
        Diagram diagram = null;
        diagram = Helper.readDisk();
        if (diagram == null){
            diagram = defaul();
        }
        diagram.print();
        if (Helper.writeDisk(diagram)){
            System.out.println("=> Save status : Success !!");
        }
    }
    public static Diagram defaul(){
        Diagram diagramDefaul = new Diagram();
        Layer layerDefaul = new Layer();
        diagramDefaul.addLayer(layerDefaul);
        Shape s1 = new Circle(2.0, 1,2);
        Shape s2 = new Circle(2.0, 1,2);
        Shape tr1 = new Triangle();
        Shape tr2 = new Triangle();
        Shape hex = new Hexagon();
        Square square1 = new Square(1,1,-1,-1);
        Square square2 = new Square(1,1,0,0);
        if (!layerDefaul.addShape(square1)) System.out.println(square1.fail());
        if (!layerDefaul.addShape(square2)) System.out.println(square2.fail());
        if (!layerDefaul.addShape(s1)) System.out.println(s1.fail());
        if (!layerDefaul.addShape(s2)) System.out.println(s2.fail());
        if (!layerDefaul.addShape(tr1)) System.out.println(tr1.fail());
        if (!layerDefaul.addShape(tr2)) System.out.println(tr2.fail());
        if (!layerDefaul.addShape(s1)) System.out.println(s1.fail());
        if (!layerDefaul.addShape(hex)) System.out.println(hex.fail());
//           layerDefaul.addShape(tr3);
//           layerDefaul.addShape(square);
        layerDefaul.removeSame();
//            layerDefaul.removeTriangle();
//            diagramDefaul.removeCircle();
        diagramDefaul.classifiShape();
        return diagramDefaul;
    }
}
