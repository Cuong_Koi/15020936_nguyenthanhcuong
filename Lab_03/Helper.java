
import java.io.*;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

class Helper {
    static double round(double a) {
        return (double) Math.round(a * 1000000) / 1000000;
    }

    static Diagram readDisk() {
        Diagram diagrams = null;
        try {
            FileInputStream input = new FileInputStream("Diagram.dat");
            ObjectInputStream obs = new ObjectInputStream(input);
            diagrams = (Diagram) obs.readObject();
            obs.close();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
        } catch (FileNotFoundException e) {
            return null;
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return diagrams;
    }

    static boolean writeDisk(Diagram diagrams) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("Diagram.dat");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(diagrams);
            objectOutputStream.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            return false;
        } catch (IOException e) {
            System.out.println("Error !!");
            return false;
        }
        return true;
    }
}
