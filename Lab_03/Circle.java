
import java.io.Serializable;

public class Circle extends Shape implements ShapeInteface, Serializable {
    private double radius;
    private Coordinates center ;

    public Circle(){
        super();
        this.radius = 1.0;
        center = new Coordinates();
    }

    public Circle(double radius, double x, double y){
        super();
        center = new Coordinates(x,y);
        this.radius = radius;
    }

    public Circle( double radius, double x, double y, String color, boolean filled){
        super(color,filled);
        center = new Coordinates(x,y);
        this.radius = radius;
    }

    public Coordinates getCenter() {
        return center;
    }

    public void setCenter(Coordinates center) {
        this.center = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Helper.round(Math.PI * Math.pow(radius, 2));
    }

    public double getPerimeter(){
        return Helper.round(2 * Math.PI * radius);
    }

    public String toString(){
        return super.toString();
    }

    @Override
    public String fail() {
        StringBuilder s = new StringBuilder();
        s.append("Radius of Circle is wrong -> radius = "+ radius);
        return s.toString();
    }

    @Override
    public boolean validate() {
        if ( radius == 0 ) return false;
        return true;
    }

    @Override
    public boolean isSame(Object b) {
        if (this == b) return true;
        if (this.getClass() != b.getClass()) return false;
        Circle circle = (Circle) b;
        if(this.radius != circle.radius) return false;
        return center.equal(circle.center);
    }

    public void print(){
        super.print();
        System.out.println("type: Circle");
        System.out.println("center: ("+this.center.getX()+":"+this.center.getY()+")");
        System.out.println("Area: "+ this.getArea()+ " - Perimeter: "+ this.getPerimeter());
    }
}
